package routes

import (
"blog-rest-api/pkg/controllers"
"github.com/gorilla/mux"
)

var RegisterPostStoreRoutes = func(router *mux.Router) {
	router.HandleFunc("/", controllers.Welcome).Methods("GET")
	router.HandleFunc("/post/", controllers.CreatePost).Methods("POST")
	router.HandleFunc("/post/", controllers.GetPost).Methods("GET")
	router.HandleFunc("/post/{postId}", controllers.GetPostById).Methods("GET")
	router.HandleFunc("/post/{postId}", controllers.UpdatePost).Methods("PUT")
	router.HandleFunc("/post/{postId}", controllers.DeletePost).Methods("DELETE")
	router.HandleFunc("/post/delete/{postId}", controllers.PermanentDeletePost).Methods("DELETE")
}
