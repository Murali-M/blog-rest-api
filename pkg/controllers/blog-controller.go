package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"blog-rest-api/pkg/models"
	"blog-rest-api/pkg/utils"
	"strconv"
)

var NewPost models.Post

func Welcome(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Welcome to new rest api!")
}

func CreatePost(w http.ResponseWriter, r *http.Request) {
	CreatePost := &models.Post{}
	utils.ParseBody(r, CreatePost)
	p:= CreatePost.CreatePost()
	res,_ := json.Marshal(p)
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func GetPost(w http.ResponseWriter, r *http.Request) {
	newPosts:= models.GetAllPosts()
	res, _ := json.Marshal(newPosts)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func GetPostById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postId := vars["postId"]
	ID, err:= strconv.ParseInt(postId, 0, 0)
	if err != nil {
		fmt.Println("Error while parsing")
	}
	postDetails, _:= models.GetPostById(ID)
	res, _ := json.Marshal(postDetails)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func UpdatePost(w http.ResponseWriter, r *http.Request) {
	var updatePost = &models.Post{}
	utils.ParseBody(r, updatePost)
	vars := mux.Vars(r)
	postId := vars["postId"]
	ID, err:= strconv.ParseInt(postId, 0, 0)
	if err != nil {
		fmt.Println("Error while parsing")
	}
	postDetails, db:= models.GetPostById(ID)
	if updatePost.Title != "" {
		postDetails.Title = updatePost.Title
	}
	if updatePost.Author != "" {
		postDetails.Author = updatePost.Author
	}
	if updatePost.Content != "" {
		postDetails.Content = updatePost.Content
	}
	if updatePost.Category != "" {
		postDetails.Category = updatePost.Category
	}
	db.Save(&postDetails)
	res, _ := json.Marshal(postDetails)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func DeletePost(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postId := vars["postId"]
	ID, err:= strconv.ParseInt(postId, 0, 0)
	if err != nil {
		fmt.Println("Error while parsing")
	}
	post:= models.DeletePost(ID)
	res, _ := json.Marshal(post)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func PermanentDeletePost(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	postId := vars["postId"]
	ID, err := strconv.ParseInt(postId, 0, 0)
	if err != nil {
		fmt.Println("Error while parsing")
	}
	post:= models.PermanentDelete(ID)
	res,_ := json.Marshal(post)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}
