package models

import (
	"github.com/jinzhu/gorm"
	"blog-rest-api/pkg/config"
)

var db *gorm.DB

type Post struct {
	gorm.Model
	//Id          string `json:"id"`
	Title        string `gorm:"" json:"title"`
	Content      string `gorm:"type:text;" json:"content"`
	Author       string `json:"author"`
	Category     string `json:"category"`
}

func init() {
	config.Connect()
	db = config.GetDB()
	db.AutoMigrate(&Post{})
}

func (p *Post) CreatePost() *Post {
	db.NewRecord(p)
	db.Create(&p)
	return p
}

func  GetAllPosts() []Post {
	var posts []Post
	db.Find(&posts)
	return posts
}

func GetPostById(Id int64) (*Post , *gorm.DB){
	var getPost Post
	db:=db.Where("ID = ?", Id).Find(&getPost)
	return &getPost, db
}

func DeletePost(ID int64) Post {
	var post Post
	db.Where("ID = ?", ID).Delete(&post)
	return post
}

func PermanentDelete(ID int64) Post {
	var post Post
	db.Unscoped().Where("ID = ?", ID).Delete(&post)
	return post
}
